<?php

namespace App\Http\Controllers;

use App\Models\Peserta;
use Illuminate\Http\Request;

class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // function __construct()
    // {
    //     $this->middleware('role_or_permission:Peserta access|Peserta create|Peserta edit|Peserta delete', ['only' => ['index','show']]);
    //     $this->middleware('role_or_permission:Peserta create', ['only' => ['create','store']]);
    //     $this->middleware('role_or_permission:Peserta edit', ['only' => ['edit','update']]);
    //     $this->middleware('role_or_permission:Peserta delete', ['only' => ['destroy']]);
    // }

    public function index (Request $request)
    {
        //$data = Peserta::all();
        //return $data;
        $search = $request -> search;
        $data = Peserta::where("nim", "LIKE", "%". $search."%")
            ->orWhere("nama", "LIKE", "%". $search."%")
            ->orWhere("prodi", "LIKE", "%". $search."%")
            ->orWhere("fakultas", "LIKE", "%". $search."%")
            ->paginate(5);
        $data -> withPath('peserta');
        $data -> appends($request -> all());
        return view('peserta.tampilan', compact('data', 'search'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $baru = new Peserta;
        return view ("peserta.tambah", compact("baru"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Peserta::create($request->all());
        return redirect('peserta');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function show(Peserta $peserta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $baru =Peserta::find($id);
        return view ("peserta.edit", compact("baru"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $baru =Peserta::find($id);
        $baru -> update ($request ->all());
        return redirect('peserta');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Peserta  $peserta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $baru =Peserta::find($id);
        $baru -> delete ();
        return redirect('peserta');
    }
}
