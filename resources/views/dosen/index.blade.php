@extends('layouts.app')

@section('content')
<div class="container">
<div class="card" >
    <div class="card-header">
        <h4>Data Dosen</h4>
        <p> Dosen di Jurusan Ilmu Komputer </p>
    </div>
    <!-- Content Wrapper START -->
    <div class="main-content">
        <!-- Content goes Here -->
        <div class="page-header no-gutters">
            <div class="row align-items-md-center mx-2 my-2">
                <div class="col-md-6">
                    <form method="GET" action="{{ url('dosen') }}">
                        <input type="text" name="keyword" value="{{ $keyword }}"/>
                        <button class="btn btn-dark btn-sm" type="submit"> Search </button>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="text-md-right m-v-10">
                        <a href="{{ route('dosen.create') }}" class="btn btn-outline-primary float-right">Tambah Data</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="m-t-25">
                <table id="data-table" class="table">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Usia</th>
                        <th>Mata Kuliah</th>
                        <th>SKS</th>
                        <th style="width: 10px; text-align: center"><i class='anticon anticon-setting'></i></th>
                    </tr>

                    @foreach ($dosen as $item)
                        <tr>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->usia }}</td>
                            <td>{{ $item->mata_kuliah }}</td>
                            <td>{{ $item->sks }}</td>
                            <td>
                                <div class="btn-group inline pull-left gap-1" data-toggle="buttons-checkbox">
                                    <a href="/dosen/{{ $item->id }}/edit" class="btn btn-outline-warning ">Edit</a>
                                    <form action="/dosen/{{ $item->id }}" method="post">
                                        @csrf
                                        @method("delete")
                                        <button class="btn btn-outline-danger m-r-5" type="submit">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Content Wrapper END -->
</div>
</div>    
    
    {{ $dosen->links() }}
@endsection
