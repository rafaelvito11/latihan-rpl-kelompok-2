<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'=>'Admin',
            'email'=>'admin@kelompok2.com',
            'password'=>bcrypt('admin123')
        ]);

        $writer = User::create([
            'name'=>'user',
            'email'=>'user@kelompok2.com',
            'password'=>bcrypt('admin123')
        ]);


        $admin_role = Role::create(['name' => 'admin']);
        $user_role = Role::create(['name' => 'user']);

        $permission = Permission::create(['name' => 'Siswa access']);
        $permission = Permission::create(['name' => 'Siswa edit']);
        $permission = Permission::create(['name' => 'Siswa create']);
        $permission = Permission::create(['name' => 'Siswa delete']);

        $permission = Permission::create(['name' => 'Dosen access']);
        $permission = Permission::create(['name' => 'Dosen edit']);
        $permission = Permission::create(['name' => 'Dosen create']);
        $permission = Permission::create(['name' => 'Dosen delete']);

        $permission = Permission::create(['name' => 'Stok access']);
        $permission = Permission::create(['name' => 'Stok edit']);
        $permission = Permission::create(['name' => 'Stok create']);
        $permission = Permission::create(['name' => 'Stok delete']);

        $permission = Permission::create(['name' => 'Peserta access']);
        $permission = Permission::create(['name' => 'Peserta edit']);
        $permission = Permission::create(['name' => 'Pesarta create']);
        $permission = Permission::create(['name' => 'Peserta delete']);

        $permission = Permission::create(['name' => 'Pegawai access']);
        $permission = Permission::create(['name' => 'Pegawai edit']);
        $permission = Permission::create(['name' => 'Pegawai create']);
        $permission = Permission::create(['name' => 'Pegawai delete']);

        $permission = Permission::create(['name' => 'Film access']);
        $permission = Permission::create(['name' => 'Film edit']);
        $permission = Permission::create(['name' => 'Film create']);
        $permission = Permission::create(['name' => 'Film delete']);

        $permission = Permission::create(['name' => 'Role access']);
        $permission = Permission::create(['name' => 'Role edit']);
        $permission = Permission::create(['name' => 'Role create']);
        $permission = Permission::create(['name' => 'Role delete']);

        $permission = Permission::create(['name' => 'User access']);
        $permission = Permission::create(['name' => 'User edit']);
        $permission = Permission::create(['name' => 'User create']);
        $permission = Permission::create(['name' => 'User delete']);

        $permission = Permission::create(['name' => 'Permission access']);
        $permission = Permission::create(['name' => 'Permission edit']);
        $permission = Permission::create(['name' => 'Permission create']);
        $permission = Permission::create(['name' => 'Permission delete']);

        $admin->assignRole($admin_role);
        $writer->assignRole($user_role);


        $admin_role->givePermissionTo(Permission::all());

    }
}
