@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Ubah Data User</h3>
            </div>
            <div class="card-body">
                <a href="{{route ('permission.index')}}" class="btn btn-primary mb-2">Kembali</a>
                <form action="{{route ('permission.update', $permission->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <ul class="list-group">
                        Nama <input type="text" name="name" required value="{{$permission->name}}">
                    </ul>
                    <button class="btn btn-primary mt-2" name="submit" type="submit">Ubah Data</button>
                </form>
            </div>
        </div>
    </div>

@endsection
