@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="float-sm-left">
                        <h2> Tampilkan</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('films.index') }}"> Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Nama:</strong>
                        {{ $film->nama }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Genre:</strong>
                        {{ $film->genre }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Tanggal Rilis:</strong>
                        {{ $film->tgl_rilis }}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Sutradara:</strong>
                        {{ $film->sutradara }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
