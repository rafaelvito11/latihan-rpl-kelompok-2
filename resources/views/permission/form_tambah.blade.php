@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Ubah Data User</h3>
            </div>
            <div class="card-body">
                <a href="{{route ('permission.index')}}" class="btn btn-primary mb-2">Kembali</a>
                <form action="{{route ('permission.store')}}" method="post">
                    @csrf
                    <ul class="list-group">
                        Nama <input type="text" name="name" required>
                    </ul>

                    <button class="btn btn-success mt-3" name="submit" type="submit">Submit</button>
                    <a href="{{route('permission.index')}}" class="btn btn-danger mt-3 ml-3">Cancel</a>
                </form>
            </div>
        </div>
    </div>

@endsection
