@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>TAMBAH DATA MAHASISWA</h2>
            </div>
            <div class="card-body">
                <form action="{{route('siswa.store')}}" method="post">
                    @csrf
                    <div class="container mt-4">
                        <div class="form-floating mb-3">
                            <label for="nim">NIM</label>
                            <input type="text" class="form-control" id="nim" name="nim">
                        </div>
                        <div class="form-floating mb-3">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="exampleSelectRounded0">Program Studi</label>
                            <select class="custom-select rounded-0" id="exampleSelectRounded0" name="prodi">
                                <option value="Teknik Informasi">Teknik Infromatika</option>
                                <option value="Sistem Informasi">Sistem Infromasi</option>
                            </select>
                        </div>
                        <button class="btn btn-primary mr-1" name="submit" type="submit">Submit</button>
                        <a href="{{route('siswa.index')}}" class="btn btn-danger">Cancel</a>
                    </div>
            </div>
            </form>
            </div>
        </div>
    </div>


@endsection
