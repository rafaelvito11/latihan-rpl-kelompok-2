<?php

namespace App\Http\Controllers;

use App\Models\film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class filmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('role_or_permission:Film access|Film create|Film edit|Film delete', ['only' => ['index','show']]);
        $this->middleware('role_or_permission:Film create', ['only' => ['create','store']]);
        $this->middleware('role_or_permission:Film edit', ['only' => ['edit','update']]);
        $this->middleware('role_or_permission:Film delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $data = film::latest()->paginate(5);

        return view('films.index',compact('data'))
           ->with('cri', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('films.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'genre' => 'required',
            'tgl_rilis' => 'required',
            'sutradara' => 'required',
        ]);

        film::create($request->all());

        return redirect()->route('films.index')
                        ->with('success','film created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function show(film $film)
    {
        return view('films.show',compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function edit(film $film)
    {
        return view('films.edit',compact('film'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, film $film)
    {
        $request->validate([
            'nama' => 'required',
            'genre' => 'required',
            'tgl_rilis' => 'required',
            'sutradara' => 'required',
        ]);

        $film->update($request->all());

        return redirect()->route('films.index')
                        ->with('success','film updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy(film $film)
    {
        $film->delete();

        return redirect()->route('films.index')
                        ->with('success','film deleted successfully');
    }
    public function cri(Request $request)
    {
        // menangkap data pencarian
        $cri = $request->cri;

        // mengambil data dari table guru sesuai pencarian data
        $data = DB::table('films')
            ->where('nama','like',"%".$cri."%")
            ->paginate();


        // mengirim data pegawai ke view index
        return view('films.index',['data' => $data]);
    }


    // public function cari(Request $request)
    // {
    //     $mhsw = DB::table('value')
    //     ->where('mhsw.index','like',"%".$cari."%")
	// 	->paginate();

    //     return view('mhsws.index',['mhsw' => $mhsw]);
    // }
    // public function search(Request $request)
    // {
    //     $keyword = $request->search;
    //     $mhsw = mhsw::where('nama', 'like', "%" . $keyword . "%")->paginate(5);
    //     return view('mhsws.index', compact('mhsw'))->with('i', (request()->input('page', 1) - 1) * 5);
    // }
}
