@extends('layouts.app')

@section('content')
<!-- page content -->
<div class="container">
<div class="card" >
    <div class="card-header">
        <h2>Data Mahasiswa Ilmu Komputer</h2>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                @can('Siswa create')
                <a href="{{route('siswa.create')}}" class="btn btn-primary mb-2"> Tambah</a>
                @endcan
            </div>
            <div class="col-md-4">
                <form action="{{route('siswa.cari')}}" method="GET">
                    <input class="form-control-sm" type="text" name="cari" placeholder="Cari Siswa ..">
                    <input class="btn btn-dark btm-sm" type="submit" value="CARI">
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>NIM</th>
                    <th>Nama Mahasiswa</th>
                    <th>Program Studi</th>
                    <th>Aksi</th>
                </tr>
                @can('Siswa access')
                @foreach($siswa as $s)
                <tr>
                    <td>{{$s->nim}}</td>
                    <td>{{$s->nama}}</td>
                    <td>{{$s->prodi}}</td>
                    <td>
                        <form action="{{route ('siswa.destroy', $s->id)  }}" method="POST">
                            <a href="{{ route ('siswa.show', $s->id) }}" class="btn btn-round btn-primary" > Tampilkan </a>
                            @can('Siswa edit')
                            <a href="{{route('siswa.edit', $s->id)}}" class="btn btn-success">Edit </a>
                            @endcan
                            @csrf
                            @method('DELETE')
                            @can('Siswa delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                            @endcan
                        </form>
                    </td>
                </tr>
                @endforeach
                @endcan
            </table>

            <br/>
            <div class="col mb-2">
                <div class="row-4">
                    Halaman : {{ $siswa->currentPage() }}
                </div>
                <div class="row-4">
                    Jumlah Data : {{ $siswa->total() }}
                </div>
                <div class="row-4">
                    Data Per Halaman : {{ $siswa->perPage() }}
                </div>
                {{ $siswa->links() }}
            </div>
    </div>
</div>
</div>
@endsection
