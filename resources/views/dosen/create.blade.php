@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card" >
        <!-- Content Wrapper START -->
        <div class="main-content">
            <div class="card-header">
                <h4>Tambah Data Dosen</h4>
            </div>
            <br>
            <div class="card-body">
                <form action="{{ route('dosen.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="formGroupExampleInput">Nama</label>
                        <input type="text" class="form-control" name="nama" placeholder="nama">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Usia</label>
                        <input type="number" class="form-control" name="usia" placeholder="usia">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Mata Kuliah</label>
                        <input type="text" class="form-control" name="mata_kuliah" placeholder="mata kuliah">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">SKS</label>
                        <input type="number" class="form-control" name="sks" placeholder="sks">
                    </div>
                    <button class="btn btn-primary m-r-5" type="submit">Tambah</button>
                </form>
            </div>
        </div>
        <!-- Content Wrapper END -->
    </div>
</div>

@endsection
