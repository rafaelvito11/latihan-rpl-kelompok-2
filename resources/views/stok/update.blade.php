@extends('layouts.app')

@section('content')
    <!-- DataTales Example -->
<div class="container">

   <!-- Page Heading -->
    <h1 class="h3 mb-4 mt-3 text-gray-800">Update Stok Barang</h1>

<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- Button trigger modal -->
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table
        class="table table-bordered"
        id="dataTable"
        width="100%"
        cellspacing="0"
      >
        <form
          action="{{url('stok/'.$edit->id)}}" method="POST"

        >
        <input type="hidden" name="_method" value="PUT">
        @csrf

          <div class="modal-body">
            <div class="form-group">
              <label for="name">Name Barang</label>
              <input
                type="text"
                value="{{$edit->name}}"
                class="form-control"
                name="name"
                placeholder="Masukkan Nama Barang"
                required
              />

            </div>
            <div class="form-group mt-3">
              <label for="name">Jumlah Barang </label>
              <input
                type="number"
                value="{{$edit->jumlah}}"
                class="form-control"
                name="jumlah"
                placeholder="Masukkan Jumlah Barang"
                required
              />
            </div>
            <div class="form-group mt-3">
              <label for="name">Tanggal Produksi</label>
              <input
                type="date"
               value="{{$edit->produksi}}"
                class="form-control"
                name="produksi"
                placeholder="Masukkan Tanggal Produksi Barang"
                required
              />
            </div>
            <div class="form-group mt-3">
              <label for="name">Tanggal Kadaluarsa Barang</label>
              <input
                type="date"
                value="{{$edit->kadaluarsa}}"
                class="form-control"
                name="kadaluarsa"
                placeholder="Masukkan Tanggal Kadaluarsa Barang"
                required
              />
            </div>
            <div class="form-group mt-3">
              <label for="name">Harga</label>
              <input
                type="number"
                value="{{$edit->harga}}"
                class="form-control"
                name="made"
                placeholder="Masukkan Asal Produksi"
                required
              />
            </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary mt-3">
                  Update
                </button>
              </div>
            </div>
          </div>
        </form>
      </table>
    </div>
  </div>
</div>
</div>
@endsection