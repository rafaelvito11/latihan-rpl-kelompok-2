@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Ubah Data User</h3>
            </div>
            <div class="card-body">
                <a href="{{route ('user.index')}}" class="btn btn-primary mb-2">Kembali</a>
                <form action="{{route ('user.update', $user->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <ul class="list-group">
                        Nama <input type="text" name="name" required value="{{$user->name}}">
                        Email <input type="text" name="email" required value="{{$user->email}}">
                        Role <select class="form-control" id="role_id" name="roles[]">
                                    @foreach($roles as $row)
                                    <option value="{{$row->id}}" {{$row->id == $user->roles[0]->id ? 'selected="selected"' : ''}}>{{$row->name}}</option>
                                    @endforeach
                        </select>
                    </ul>
                    <button class="btn btn-primary mt-3" name="submit" type="submit">Ubah Data</button>
                </form>
            </div>
        </div>
    </div>

@endsection
