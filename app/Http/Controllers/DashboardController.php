<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $siswa = DB::table('siswa')->paginate(5);
        $dosen = DB::table('dosens')->paginate(5);
        $pegawai = DB::table('pegawais')->paginate(5);
        $stok = DB::table('stok')->paginate(5);
        $peserta = DB::table('peserta')->paginate(5);
        $film = DB::table('films')->paginate(5);
        return view('welcome',compact('siswa','dosen','pegawai','stok','peserta','film'));
    }
}
