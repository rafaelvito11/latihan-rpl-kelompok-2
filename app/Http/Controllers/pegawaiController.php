<?php

namespace App\Http\Controllers;

use App\Models\pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class pegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('role_or_permission:Pegawai access|Pegawai create|Pegawai edit|Pegawai delete', ['only' => ['index','show']]);
        $this->middleware('role_or_permission:Pegawai create', ['only' => ['create','store']]);
        $this->middleware('role_or_permission:Pegawai edit', ['only' => ['edit','update']]);
        $this->middleware('role_or_permission:Pegawai delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $data = DB::table('pegawais')->paginate(5);
        return view('pegawais.index',compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pegawais.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nik' => 'required',
            'tgl_lahir' => 'required',
            'jabatan' => 'required',
        ]);

        pegawai::create($request->all());

        return redirect()->route('pegawais.index')
            ->with('success','data berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function show(pegawai $pegawai)
    {
        return view('pegawais.show',compact('pegawai'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function edit(pegawai $pegawai)
    {
        return view('pegawais.edit',compact('pegawai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pegawai $pegawai)
    {
        $request->validate([
            'nama' => 'required',
            'nik' => 'required',
            'tgl_lahir' => 'required',
            'jabatan' => 'required',
        ]);

        $pegawai->update($request->all());

        return redirect()->route('pegawais.index')
            ->with('success',' Data pegawai berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pegawai  $pegawai
     * @return \Illuminate\Http\Response
     */
    public function destroy(pegawai $pegawai)
    {
        $pegawai->delete();

        return redirect()->route('pegawais.index')
            ->with('success','Data pegawai berhasil dihapus');
    }
    public function search(Request $request)
    {
        // menangkap data pencarian
        $search = $request->search;

        // mengambil data dari table guru sesuai pencarian data
        $data = DB::table('pegawais')
            ->where('nama','like',"%".$search."%")
            ->orWhere('nik','like',"%".$search."%")
            ->orWhere('jabatan','like',"%".$search."%")
            ->paginate();


        // mengirim data pegawai ke view index
        return view('pegawais.index',['data' => $data]);
    }
}
