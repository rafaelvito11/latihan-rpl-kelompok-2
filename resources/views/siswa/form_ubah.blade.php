@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>PENDATAAN MAHASISWA ILMU KOMPUTER</h2>
            </div>
            <div class="card-body">
                <form action="{{route('siswa.update', $siswa->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="container mt-4">
                        <div class="form-floating mb-3">
                            <label for="nim">NIM</label>
                            <input type="text" class="form-control" id="nim" name="nim" required value="{{$siswa->nim}}" >
                        </div>
                        <div class="form-floating mb-3">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" required value="{{$siswa->nama}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleSelectRounded0">Program Studi</label>
                            <select class="custom-select rounded-0" id="exampleSelectRounded0" name="prodi">
                                <option selected>{{$siswa->prodi}}</option>
                                <option value="Teknik Informasi">Teknik Infromatika</option>
                                <option value="Sistem Informasi">Sistem Infromasi</option>
                            </select>
                        </div>
                        <button class="btn btn-primary mr-1" name="submit" type="submit">Submit</button>
                        <a href="{{route('siswa.index')}}" class="btn btn-danger">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection
