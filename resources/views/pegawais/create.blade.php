@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="card-header">
                    <div class="pull-left">
                        <h2>Add</h2>
                    </div>
                </div>
            </div>
        </div>
        
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card-body">
            <form action="{{ route('pegawais.store') }}" method="POST">
                @csrf
                
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Nama:</strong>
                            <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>NIK:</strong>
                            <input type="double" name="nik" class="form-control" placeholder="Masukkan NIK">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Tanggal Lahir:</strong>
                            <input type="date" name="tgl_lahir" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Jabatan:</strong>
                            <input type="text" name="jabatan" class="form-control" placeholder="Masukkan Jabatan">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-3 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a class="btn btn-danger" href="{{ route('pegawais.index') }}"> Back</a>
                    </div>
                    
            </form>
        </div>
        @endsection
    </div>
</div>
    