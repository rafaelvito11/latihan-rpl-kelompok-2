<?php

use App\Http\Controllers\DosenController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StokController;
use App\Http\Controllers\filmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();

Route::resource('siswa', \App\Http\Controllers\SiswaController::class);
Route::get('cari', [\App\Http\Controllers\SiswaController::class, 'cari'])->name('siswa.cari');
Route::resource('pegawais', \App\Http\Controllers\pegawaiController::class);
Route::get('search', [\App\Http\Controllers\pegawaiController::class, 'search'])->name('pegawais.search');
Route::resource('films', filmController::class);
Route::get('cri', [filmController::class, 'cri'])->name('films.cri');
Route::resource('stok', StokController::class);
Route::resource('peserta', \App\Http\Controllers\PesertaController::class);
Route::resource('user', \App\Http\Controllers\UserController::class);
// Route::get('/dashboard', function () {return view('welcome');})->name('dashboard');
Route::resource('dosen', DosenController::class);
Route::resource('role',\App\Http\Controllers\RoleController::class);
Route::resource('permission',\App\Http\Controllers\PermissionController::class);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('auth/google',[\App\Http\Controllers\GoogleController::class,'redirectToGoogle'])->name('google.login');
Route::get('auth/google/callback',[\App\Http\Controllers\GoogleController::class,'handleGoogleCallback'])->name('google.callback');

Route::get('dashboard',[\App\Http\Controllers\DashboardController::class,'index'])->name('dashboard');

