@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card"> 
        <div class="card-header">
            <h3>Daftar Data Pegawai</h3>
        </div>
        <div class="card-body">
            @can('User create')
            <div class="row">
                 <div class="col md -4">
                    <a class="btn btn-success" href="{{ route('pegawais.create') }}"> Create</a>
            </div>
            @endcan
            <div class="col-md-16" style="float:right;">
                <form action={{ route('pegawais.search') }} method="GET">
                    <label>
                        <input type="text" placeholder="Cari..." name="search" style="border-color:#0d6efd; height: 35px; ">
                    </label>
                    <button type="submit" class="btn btn-primary" >
                        <i class="fa fa-search mt-1"type="submit" ></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>            
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        
        <table class="table table-bordered">
            <tr align="center">
                <th>Nama</th>
                <th>NIK</th>
                <th>Tanggal Lahir</th>
                <th>Jabatan</th>
                <th width="250px">Action</th>
            </tr>
            @can('User access')
            @foreach ($data as $key => $value)
            <tr align="center">
                <td align="left">{{ $value->nama }}</td>
                <td>{{ $value->nik}}</td>
                <td>{{ $value->tgl_lahir }}</td>
                <td>{{ $value->jabatan }}</td>
                <td>
                    <form action="{{ route('pegawais.destroy',$value->id) }}" method="POST">
                        <a class="btn btn-primary" href="{{ route('pegawais.edit',$value->id) }}">Edit</a>
                        <a class="btn btn-info" href="{{ route('pegawais.show',$value->id) }}" style="margin-left: 1.3rem">Show</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" > Delete </button>
                        
                    </form>
                </td>
            </tr>
            @endforeach
            @endcan
        </table>
        
        <a class="btn btn-primary pull-right" style="margin-top:1rem; " href="{{ route('pegawais.index') }}"> Back</a>
        {{ $data->links() }}
    </div>
</div>
    
    @endsection
    