<?php

namespace App\Http\Controllers;

use App\Models\Stok;
use Illuminate\Http\Request;

class StokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('role_or_permission:Stok access|Stok create|Stok edit|Stok delete', ['only' => ['index','show']]);
        $this->middleware('role_or_permission:Stok create', ['only' => ['create','store']]);
        $this->middleware('role_or_permission:Stok edit', ['only' => ['edit','update']]);
        $this->middleware('role_or_permission:Stok delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
        // $stok = Stok::all();
        // return $stok;
        $keyword =$request->keyword;
        $stok = Stok::where('name', 'LIKE', '%'.$keyword.'%')
            ->orWhere('jumlah', 'LIKE', '%'.$keyword.'%')
            ->orWhere('produksi', 'LIKE', '%'.$keyword.'%')
            ->orWhere('kadaluarsa', 'LIKE', '%'.$keyword.'%')
            ->orWhere('harga', 'LIKE', '%'.$keyword.'%')
            ->paginate(5);
        $stok->withPath('stok');
        $stok->appends($request->all());
        return view('stok.show', compact('stok', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $new = new Stok;
        return view('stok.create', compact('new'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Stok::create($request->all());
        return redirect('stok')->with('success', "Add Data Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function show(Stok $stok)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Stok::find($id);
        return view('stok.update', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stok = Stok::find($id);
        $stok->update($request->all());
        return redirect('stok')->with('success', "Update Data Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stok  $stok
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $edit = Stok::find($id);
        $edit->delete();

        return redirect('stok')->with('success', "Delete Data Success");
    }
}
