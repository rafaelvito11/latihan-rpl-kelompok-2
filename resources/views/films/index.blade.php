@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h2 style="font: bold">DAFTAR FILM</h2>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                    <div class="pull-left">
                    <a class="btn btn-success mb-2" href="{{ route('films.create') }}"> Tambah</a>
                    </div>
                    <div class="pull-right ">
                    </div>
            </div>
            <div class="col-md-4">
                    <form action={{ route('films.cri') }} method="GET">
                        <label>
                            <input type="text" name="cri">
                        </label>
                        <input type="submit" value="CARI">
                    </form> 
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        
        <table class="table table-bordered">
            <tr>
                <th>Nama</th>
                <th>Genre</th>
                <th>Tanggal Rilis</th>
                <th>Sutradara</th>
                <th width="250px">Action</th>
            </tr>
            @foreach ($data as $key => $value)
                <tr>
                <td>{{ $value->nama }}</td>
                <td>{{ $value->genre}}</td>
                <td>{{ $value->tgl_rilis }}</td>
                <td>{{ $value->sutradara }}</td>
                <td>
                    <form action="{{ route('films.destroy',$value->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('films.show',$value->id) }}">Lihat</a>
                        <a class="btn btn-primary" href="{{ route('films.edit',$value->id) }}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>




        {{ $data->links() }}
    </div>

@endsection
