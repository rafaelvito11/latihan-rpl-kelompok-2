@extends('layouts.app')

@section('content')
<h1 class="h3 mb-4 text-gray-800"><center>DATA PESERTA</center></h1>
<div class="row justify-content-center mt-5">
<div class="col-8">

    
    <form action="{{url('peserta')}}" method="get" class="float-right mb-3">
      <input type="text" name="search" class="d-inline" value="{{$search}}"/>
      <button type="submit">
          Search
      </button>
  </form>    
  <a href="{{url('peserta/create')}}" type="button" class="btn btn-secondary btn-sm mb-3 ">
    Tambah
</a>
@csrf
<table class="table table-dark table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nim</th>
        <th scope="col">Nama</th>
        <th scope="col">Prodi</th>
        <th scope="col">Fakultas</th>
        <th scope="col">action</th>
      </tr>
    </thead>
        @foreach ($data as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->nim}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->prodi}}</td>
            <td>{{$item->fakultas}}</td>
        
            <td>
              
                <a href="{{url('peserta/'.$item->id.'/edit')}}" class="btn btn-primary btn-sm button-update">
                  Edit </a>
            
                <form action="{{url('peserta/'.$item->id)}}" method="POST" class="d-inline">
                    <input type="hidden" value="DELETE" name="_method">
                    @csrf
                    <input type="hidden" name=_method value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm">
                 Hapus
                </button>
                </form>
            </td>

        </tr>
            
        @endforeach
        
    <tbody>

      
    </tbody>
  </table>
  <div class="">
    <div class="d-flex justify content-end">
      {{$data ->links()}}
    </div>
  </div>
</div>

</div>

@endsection
