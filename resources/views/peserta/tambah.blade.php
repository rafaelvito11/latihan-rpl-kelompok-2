@extends('layouts.app')
@section('content')
    
<h1 class="h3 mb-4 text-gray-800"><center>TAMBAH DATA PESERTA</center></h1>
<div class="row justify-content-center mt-5">
<div class="col-8">
    
  <form action="{{url('peserta')}}" method="POST">
    @csrf
    <div class="mb-3">
      <label>NIM</label>
      <input type="number" class="form-control" name="nim" placeholder="Masukkan NIM">
    </div>
    <div class="mb-3">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Lengkap">
    </div>
    <div class="mb-3">
      <label>Prodi</label>
      <input type="text" class="form-control" name="prodi" placeholder="Masukkan Prodi">
    </div>
    <div class="mb-3">
      <label>Fakultas</label>
      <input type="text" class="form-control" name="fakultas" placeholder="Masukkan Nama Fakultas">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

</div>
</div>
@endsection