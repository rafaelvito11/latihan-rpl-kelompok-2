@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Role Management</h3>
            </div>

            <div class="card-body">
                @can('Role create')
                    <div class="row mb-2">
                        <div class="col-md-3">
                            <a href="{{route('role.create')}}" class="btn btn-primary"> Tambah</a>
                        </div>
                    </div>
                @endcan
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Nama Role</th>
                        <th>Permission</th>
                        <th>Aksi</th>
                    </tr>

                    @can('User access')
                    @foreach($roles as $r)
                        <tr>
                            <td>{{ $r->name }}</td>
                            <td>
                                @foreach($r->permissions as $p)
                                    <button  class="btn btn btn-outline-dark btn-sm mt-2 ml-2 r-2" disabled data-bs-toggle="button">{{$p->name}}</button>
                                @endforeach
                            </td>
                            <td>
                                <ul class="nav">
                                    <form action="{{route ('role.destroy', $r->id)  }}" method="POST">
                                        @can('Role edit')
                                        <a href="{{ route ('role.edit', $r->id) }}" class="btn btn-primary mr-1 mb-1">Edit</a>
                                        @endcan
                                        @csrf
                                        @method('DELETE')
                                        @can('Role delete')
                                        <Button type="submit" class="btn btn-danger">Delete</Button>
                                        @endcan
                                    </form>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    @endcan
                </table>
            </div>
        </div>
    </div>
@endsection
