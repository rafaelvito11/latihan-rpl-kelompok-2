<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('role_or_permission:Siswa access|Siswa create|Siswa edit|Siswa delete', ['only' => ['index','show']]);
        $this->middleware('role_or_permission:Siswa create', ['only' => ['create','store']]);
        $this->middleware('role_or_permission:Siswa edit', ['only' => ['edit','update']]);
        $this->middleware('role_or_permission:Siswa delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $datasiswa = DB::table('siswa')->paginate(5);
        return view('siswa.index',['siswa' => $datasiswa]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswa.form_tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Siswa::create([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'prodi' => $request->prodi
        ]);

        return redirect()->route('siswa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $siswa = Siswa::where('id',$id)->first();
        return view('siswa.profil_siswa',['siswa' => $siswa]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datasiswa = Siswa::find($id);
        return view('siswa.form_ubah', ['siswa'=>$datasiswa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $siswa= Siswa::find($id);
        $siswa->nim = $request->nim;
        $siswa->nama = $request->nama;
        $siswa->prodi = $request->prodi;
        $siswa->save();

        return redirect()->route('siswa.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    // public function destroy($id)
    // {
    //     $siswa = Siswa::find($id);
    //     $siswa->delete();

    //     return redirect()->route('siswa.index');
    // }
    public function destroy(Siswa $siswa)
    {
        $siswa->delete();

        return redirect()->route('siswa.index');
    }
    public function cari(Request $request)
        {
             // menangkap data pencarian
             $cari = $request->cari;

             // mengambil data dari table guru sesuai pencarian data
             $datasiswa = DB::table('siswa')
             ->where('nama','like',"%".$cari."%")
             ->paginate(5);

             // mengirim data pegawai ke view index
             return view('siswa.index',['siswa' => $datasiswa]);

        }
}
