@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-conten-center">
        <div class="card w-50">
            <div class="card-header">
                <h3>Profil Siswa</h3>
            </div>
            <div class="card-body">
                <a href="{{route ('siswa.index')}}">Kembali</a>
                <div class="card-body">
                    <div class="row ml-4">
                        <h4 class="col-6">Nama</h4>
                        <h4 class="col-6" >: {{$siswa->nama}}</h4>
                    </div>
                    <div class="row ml-4">
                        <h4 class="col-6">NIM</h4>
                        <h4 class="col-6" >: {{$siswa->nim}}</h4>
                    </div>
                    <div class="row ml-4">
                        <h4 class="col-6">Prodi</h4>
                        <h4 class="col-6">: {{$siswa->prodi}}</h4>
                    </div>
                </div>
            </div>
        </div>


@endsection
