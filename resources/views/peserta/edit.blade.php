@extends('layouts.app')
@section('content')
<h1 class="h3 mb-4 text-gray-800"><center>EDIT DATA PESERTA</center></h1>
<div class="row justify-content-center mt-5">
<div class="col-8">
    
  <form action="{{url('peserta/'.$baru->id)}}" method="POST">
    <input type="hidden" name="_method" value="PUT">
    @csrf
    
    <div class="mb-3">
      <label>NIM</label>
      <input type="number" class="form-control" name="nim" placeholder="Masukkan NIM" value="{{$baru->nim}}">
    </div>
    <div class="mb-3">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Lengkap" value="{{$baru->nama}}">
    </div>
    <div class="mb-3">
      <label>Prodi</label>
      <input type="text" class="form-control" name="prodi" placeholder="Masukkan Prodi" value="{{$baru->prodi}}">
    </div>
    <div class="mb-3">
      <label>Fakultas</label>
      <input type="text" class="form-control" name="fakultas" placeholder="Masukkan Nama Fakultas" value="{{$baru->fakultas}}">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

</div>
</div>
    
@endsection