@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Permission Data</h3>
            </div>
            <div class="card-body">
                <div class="row mt-2 mb-2">
                    <div class="col-md-3">
                            @can('Permission create')
                            <a href="{{route('permission.create')}}" class="btn btn-primary"> Tambah</a>
                            @endcan
                        </div>
                    </div>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>

                    @can('Permission access')
                    @foreach($permissions as $p)
                        <tr>
                            <td>{{ $p->name }}</td>
                            <td>
                                <ul class="nav">
                                    <form action="{{route ('permission.destroy', $p->id)  }}" method="POST">
                                        @can('Permission edit')
                                        <a href="{{ route ('permission.edit', $p->id) }}" class="btn btn-primary mr-2">Edit</a>
                                        @endcan
                                        @csrf
                                        @method('DELETE')
                                        @can('Permission delete')
                                        <Button class="btn btn-danger">Delete</Button>
                                        @endcan
                                    </form>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    @endcan
                </table>
            </div>
        </div>
    </div>
@endsection
