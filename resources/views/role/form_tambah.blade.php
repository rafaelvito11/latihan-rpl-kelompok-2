@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Ubah Data User</h3>
            </div>
            <div class="card-body">
                <a href="{{route ('role.index')}}" class="btn btn-primary mb-2">Kembali</a>
                <form action="{{route ('role.store')}}" method="post">
                    @csrf
                    <ul class="list-group">
                        <label  for="name">Nama</label>
                        <input class="mb-3" type="text" name="name" id="name" required>

                        <label for="permission">Permisiion</label>
                            @foreach($permissions as $permission)
                            <div class="flex flex-col justify-cente">
                                <div class="flex flex-col">
                                    <label class="inline-flex items-center ">
                                        <input type="checkbox" class="form-checkbox h-5 w-5 text-blue-600" name="permissions[]" id="permission" value="{{$permission->id}}"
                                        ><span class="ml-2 text-gray-700">{{ $permission->name }}</span>
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </ul>

                    <button class="btn btn-success" name="submit" type="submit">Submit</button>
                    <a href="{{route('role.index')}}" class="btn btn-danger">Cancel</a>
                </form>
            </div>
        </div>
    </div>

@endsection
