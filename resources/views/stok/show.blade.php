@extends('layouts.app')

@section('content')
<div class="container">
     <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Data Stok Barang Ilmu Komputer</h1>



    @if (Session::has('success'))
        <p class="alert alert-warning">{{Session::get('success')}}</p>
    @endif

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">

    <div class="row justify-content-between">
    <div class="col-8">
      @can('Stok create')
      <!-- Button trigger modal -->
    <a href="{{url('stok/create')}}" type="button" class="btn btn-secondary ">
      <i class="fas fa-plus"></i>
      
    </a>
    @endcan
    </div>
    <div class="col-4">
    {{-- Searchbar  --}}
    <form action="{{url('stok')}}" method="get" class="d-flex">
        <input type="text" name="keyword" class=" form-control me-2" value="{{$keyword}}" placeholder="Search keyword"/>
        <button class="btn btn-success btn-sm" type="submit">
            <i class="fas fa-search"></i>
            
        </button>
    </form>
    </div>
  </div>

    
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table
        class="table table-bordered table-hover"
        id="dataTable"
        width="100%"
        cellspacing="0"
      >

        @csrf


        <thead>
          <tr>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Tanggal Produksi</th>
            <th>Kadaluarsa Produk</th>
            <th>Harga</th>
            <th>Action</th>
          </tr>
        </thead>
        @can('Stok access')
        @foreach ($stok as $barang)
        <tr>
            <td>{{$barang->name}}</td>
            <td>{{$barang->jumlah}}</td>
            <td>{{$barang->produksi}}</td>
            <td>{{$barang->kadaluarsa}}</td>
            <td>{{$barang->harga}}</td>
            <td>
              {{-- Edit --}}
                @can('Stok edit')
                <a href="{{url('stok/'.$barang->id.'/edit')}}" class="btn btn-primary btn-sm button-update mt-1">
                  <i class="fas fa-edit"></i>
                  
                </a>
                @endcan
                @can('Stok delete')
                {{-- Delete --}}
                <form action="{{url('stok/'.$barang->id)}}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm mt-1">
                  <i class="fas fa-trash"></i>
                  
                </button>
                </form>
                @endcan
            </td>
        </tr>

        
        @endforeach
        @endcan
        
      </table>
      </div>
            <div class="d-flex justify-content-end">
            {{$stok->links()}}
        </div>
    </div>
  </div>
  </div>
  


@endsection