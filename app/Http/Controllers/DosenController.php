<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PhpParser\Builder\Function_;
use PhpParser\Node\Expr\FuncCall;
use Illuminate\Support\Facades\DB;
use App\Models\Dosen;


class DosenController extends Controller
{
    function __construct()
    {
        $this->middleware('role_or_permission:Dosen access|Dosen create|Dosen edit|Dosen delete', ['only' => ['index','show']]);
        $this->middleware('role_or_permission:Dosen create', ['only' => ['create','store']]);
        $this->middleware('role_or_permission:Dosen edit', ['only' => ['edit','update']]);
        $this->middleware('role_or_permission:Dosen delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $keyword = $request->keyword;
        // $dosen = DB::table('dosen')->get();
        $dosen = Dosen::where('nama', 'LIKE', '%'.$keyword.'%')
            ->orwhere('usia', 'LIKE', '%'.$keyword.'%')
            ->paginate(5);
        $dosen->withPath('dosen');
        $dosen->appends($request->all());
        // dd($dosen);
        return view ('dosen.index', compact('dosen', 'keyword'));
    }

    public function create()
    {
        return view ('dosen.create');
    }

    public function store(Request $request)
    {
        Dosen::create($request->all());

        return redirect('/dosen');
    }

    public function edit($id)
    {
        // $dosen = Dosen::where('id', $id)->first();
        $dosen = Dosen::find($id);
        return view('dosen.edit', ['dosen' => $dosen]);
    }

    public function update(Request $request, $id)
    {
        $dosen = Dosen::find($id)->update([
            'nama' => $request->nama,
            'usia' => $request->usia,
            'mata_kuliah' => $request->mata_kuliah,
            'sks' => $request->sks
        ]);
        return redirect('/dosen');
    }

    public function destroy($id)
    {
        $dosen = Dosen::find($id)->delete();

        return redirect('/dosen');
    }


}
