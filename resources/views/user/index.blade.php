@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Manajemen User</h3>
            </div>
            <div class="card-body">
                @can('User create')
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <a href="{{route('user.create')}}" class="btn btn-primary"> Tambah</a>
                        </div>
                    </div>
                @endcan
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Aksi</th>
                    </tr>
                    @can('User access')
                    @foreach($user as $u)
                        <tr>
                            <td>{{ $u->name }}</td>
                            <td>{{ $u->email }}</td>
                            <td>
                                @foreach($u->roles as $role)
                                    <span> {{$role->name}}</span>
                                @endforeach
                            </td>
                            <td>
                                <ul class="nav">
                                    <form action="{{route ('user.destroy', $u->id)  }}" method="POST">
                                        @can('User edit')
                                        <a href="{{ route ('user.edit', $u->id) }}" class="btn btn-primary mr-1">Edit</a>
                                        @endcan
                                        @csrf
                                        @method('DELETE')
                                        @can('User delete')
                                        <Button type="submit" class="btn btn-danger">Delete</Button>
                                        @endcan
                                    </form>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    @endcan
                </table>
            </div>
        </div>
    </div>
@endsection
