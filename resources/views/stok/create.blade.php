@extends('layouts.app')

@section('content')
<div class="container">
     <!-- Page Heading -->
    <h1 class="h3 mb-4 mt-3 text-gray-800">Create Stok Barang</h1>

<!-- DataTales Example -->

<div class="card shadow mb-4">
  <div class="card-header py-3">
    <!-- Button trigger modal -->
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table
        class="table table-bordered"
        id="dataTable"
        width="100%"
        cellspacing="0"
      >
        <form
          action="{{url('stok')}}" method="POST"
        >
        @csrf

          <div class="modal-body">
            <div class="form-group">
              <label for="name">Name Barang</label>
              <input
                type="text"
                value=""
                class="form-control"
                name="name"
                placeholder="Masukkan Nama Barang"
                required
              />

            </div>
            <div class="form-group mt-3">
              <label for="name">Jumlah Barang </label>
              <input
                type="number"
                value=""
                class="form-control"
                name="jumlah"
                placeholder="Masukkan Jumlah Barang"
                required
              />
            </div>
            <div class="form-group mt-3">
              <label for="name">Tanggal Produksi</label>
              <input
                type="date"
                value=""
                class="form-control"
                name="produksi"
                placeholder="Masukkan Tanggal Produksi Barang"
                required
              />
            </div>
            <div class="form-group mt-3">
              <label for="name">Tanggal Kadaluarsa Barang</label>
              <input
                type="date"
                value=""
                class="form-control"
                name="kadaluarsa"
                placeholder="Masukkan Tanggal Kadaluarsa Barang"
                required
              />
            </div>
            <div class="form-group mt-3">
              <label for="name">Harga</label>
              <input
                type="number"
                value=""
                class="form-control"
                name="harga"
                placeholder="Masukkan Harga Barang"
                required
              />
            </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary mt-3">
                  Add Stock
                </button>
              </div>
            </div>
          </div>
        </form>
      </table>
    </div>
  </div>
</div>
</div>




@endsection